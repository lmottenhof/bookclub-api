using System;
using System.Collections.Generic;
using BookClubData.Interfaces;
using BookClubLogic.Interfaces;
using Models;

namespace BookClubLogic.Implementations
{
    public class ForumLogic : IForumLogic
    {
        private readonly IForumRepository forumRepo;
        public ForumLogic(IForumRepository forumRepo)
        {
            this.forumRepo = forumRepo;
        }

        public void CreatePost(int bookId, string author, string title, string text) => this.forumRepo.Insert(bookId, author, title, text, DateTime.Now);
        public IEnumerable<ForumPost> GetForumPosts(int bookId, int page) => this.forumRepo.GetPosts(bookId, page);
        public int GetPageCount(int bookId) => this.forumRepo.GetPageCount(bookId);
    }
}