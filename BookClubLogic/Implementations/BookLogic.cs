﻿using System;
using System.Collections.Generic;
using BookClubData.Interfaces;
using BookClubLogic.Interfaces;
using Models;
using Models.Exceptions;
using ORM;

namespace BookClubLogic.Implementations
{
    public class BookLogic : IBookLogic
    {
        private readonly IBookRepository bookRepo;
        private readonly IReviewRepository reviewRepo;
        
        public BookLogic(IBookRepository bookRepo, IReviewRepository reviewRepo)
        {
            this.bookRepo = bookRepo;
            this.reviewRepo = reviewRepo;
        }
        
        public IEnumerable<Book> GetBooks(int page) => bookRepo.GetPage(page);

        public int GetPageCount() => bookRepo.GetPageCount();

        public BookDetail GetDetail(int id)
        {
            Book? book = bookRepo.GetById(id);
            if (book == null) throw new ResourceNotFoundException("Could not find book with given book Id");

            ReviewData reviewData = this.reviewRepo.GetReviewData(id);
            
            return new BookDetail()
            {
                Authors = book.Authors,
                Id = book.Id,
                Isbn = book.Isbn,
                Isbn13 = book.Isbn13,
                Language = book.Language,
                PageCount = book.PageCount,
                Title = book.Title,
                AverageRating = reviewData.AverageRating,
                ReviewCount = reviewData.ReviewCount
            };
        } 
    }
}
