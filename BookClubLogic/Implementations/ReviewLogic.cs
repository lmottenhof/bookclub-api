using System;
using System.Collections.Generic;
using BookClubData.Implementations;
using BookClubData.Interfaces;
using BookClubLogic.Interfaces;
using Models;
using Models.Requests;
using ORM;

namespace BookClubLogic.Implementations
{
    public class ReviewLogic : IReviewLogic
    {
        private IReviewRepository bookRepo;
        
        public ReviewLogic(IReviewRepository bookRepo)
        {
            this.bookRepo = bookRepo;
        }

        public void AddReview(string userId, int bookId, ReviewRequest review) => this.bookRepo.Insert(userId, bookId, review, DateTime.Now);
        public IEnumerable<BookReview> GetReviews(int page, int bookId) => this.bookRepo.GetByBookId(page, bookId);
        public void DeleteReview(int bookId) => this.bookRepo.Delete(bookId);
        public int GetPageCount(int bookId) => this.bookRepo.GetPageCount(bookId);
    }
}