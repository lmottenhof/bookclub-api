using System.Collections.Generic;
using Models;
using Models.Requests;

namespace BookClubLogic.Interfaces
{
    public interface IReviewLogic
    {
        public void AddReview(string userId, int bookId, ReviewRequest review);
        public IEnumerable<BookReview> GetReviews(int page, int bookId);
        public void DeleteReview(int bookId);
        int GetPageCount(int bookId);
    }
}