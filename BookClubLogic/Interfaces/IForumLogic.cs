using System;
using System.Collections.Generic;
using Models;

namespace BookClubLogic.Interfaces
{
    public interface IForumLogic
    {
        void CreatePost(int bookId, string author, string title, string text);
        IEnumerable<ForumPost> GetForumPosts(int bookId, int page);
        int GetPageCount(int bookId);
    }
}