﻿using System.Collections.Generic;
using Models;

namespace BookClubLogic.Interfaces
{
    public interface IBookLogic
    {
        IEnumerable<Book> GetBooks(int page);
        int GetPageCount();
        BookDetail GetDetail(int id);
    }
}