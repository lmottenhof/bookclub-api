using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.WebSockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using BookClub_API.Controllers;
using BookClubData.Implementations;
using BookClubData.Interfaces;
using BookClubLogic.Implementations;
using BookClubLogic.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Models;
using Newtonsoft.Json;
using Npgsql;
using ORM;
using ORM.Repositories;

namespace BookClub_API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IServiceCollection services;

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            this.services = services;
            int pageSize = Convert.ToInt32(Configuration["PageSize"]);
            string connectionString = Configuration["ConnectionString"];
            string authenticationUrl = Configuration["AuthenticationURL"];


            NpgsqlConnection conn = new NpgsqlConnection(connectionString);
            DatabaseContext context = new DatabaseContext(conn);
            services.AddSingleton(typeof(IBookRepository), serv => new BookRepository(context, pageSize));
            services.AddSingleton(typeof(IReviewRepository), serv => new ReviewRepository(context, pageSize));
            services.AddSingleton(typeof(IForumRepository), serv => new ForumRepository(context, pageSize));
            services.AddSingleton<IBookLogic, BookLogic>();
            services.AddSingleton<IReviewLogic, ReviewLogic>();
            services.AddSingleton<IForumLogic, ForumLogic>();
            services.AddSingleton<WebSocketHandler>();
            services.AddControllers();

            /*HttpClient client = new HttpClient();
            string result = client.GetAsync(authenticationUrl + "/auth/key").Result.Content.ReadAsStringAsync().Result;
            string key = JsonConvert.DeserializeObject<KeyResponse>(result).Publickey;
            RSA publicRsa = RSA.Create();
            publicRsa.ImportRSAPublicKey(Encoding.UTF8.GetBytes(key), out int _);
            RsaSecurityKey signingKey = new RsaSecurityKey(publicRsa);*/
            
            /*services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        //ValidateIssuer = true,
                        //ValidateAudience = true,
                        //ValidateLifetime = true,
                        //ValidateIssuerSigningKey = true,
                        //ValidIssuer = Configuration["Jwt:Issuer"],
                        //ValidAudience = Configuration["Jwt:Issuer"],
                        //IssuerSigningKey = signingKey
                    };
                });*/
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder =>
            {
                builder.AllowAnyOrigin();
                builder.AllowAnyMethod();
                builder.AllowAnyHeader();
            });
            
            WebSocketOptions webSocketOptions = new WebSocketOptions() 
            {
                KeepAliveInterval = TimeSpan.FromSeconds(10),
                ReceiveBufferSize = 4 * 1024
            };

            app.UseWebSockets(webSocketOptions);

           


            //app.UseHttpsRedirection();
            //app.UseAuthentication();
            app.UseRouting();
            //app.UseAuthorization();
            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });
        }
    }

    class KeyResponse
    {
        public string Publickey { get; set; }
    }
}