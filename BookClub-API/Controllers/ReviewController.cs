using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BookClubLogic.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Models.Requests;

namespace BookClub_API.Controllers
{
    [ApiController]
    [Route("book/{bookId}/reviews")]
    public class ReviewController : ControllerBase
    {
        private readonly IReviewLogic reviewlogic;
        private readonly ILogger logger;
        
        public ReviewController(ILogger<ReviewController> logger, IReviewLogic reviewlogic)
        {
            this.reviewlogic = reviewlogic;
            this.logger = logger;
        }

        [HttpPost("/book/{bookId:int}/reviews")]
        public async Task<IActionResult> PostReview(int bookId, [FromBody] ReviewRequest request)
        {
            //string email = this.User.Claims.Single(c => c.Type == "Email").Value;
            
            try
            {
                this.reviewlogic.AddReview("testUser", bookId, request);
                return Ok();
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "");
                return BadRequest();
            }
        }
        
        [HttpGet("/book/{bookId}/reviews/{page}")]
        public async Task<IActionResult> GetReviews(uint page, int bookId)
        {
            try
            {
                return Ok(new
                {
                    reviews = this.reviewlogic.GetReviews((int)page, bookId).Select(rev => new
                    {
                        rev.Author, 
                        rev.Id,
                        rev.Rating,
                        rev.Text,
                        rev.BookId,
                        PostTime = rev.PostTime.ToString("dddd dd MMMM HH:mm")
                    }),
                    Page = page,
                    MaxPage = this.reviewlogic.GetPageCount(bookId)
                });
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "");
                return BadRequest();
            }
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteReview(int bookId)
        {
            try
            {
                this.reviewlogic.DeleteReview(bookId);
                return Ok();
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "");
                return BadRequest();
            }
        }
    }
}