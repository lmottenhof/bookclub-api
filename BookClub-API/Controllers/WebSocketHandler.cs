using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Connections;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace BookClub_API.Controllers
{
    public class WebSocketHandler : Controller
    {
        private readonly ILogger<WebSocketHandler> logger;
        
        private static readonly ConcurrentDictionary<WebSocket, int> Rooms = new ConcurrentDictionary<WebSocket, int>();
        
        public WebSocketHandler(ILogger<WebSocketHandler> logger)
        {
            this.logger = logger;
        }

        [HttpGet("/book/{bookId}/ws/info")]
        public async Task<IActionResult> GetRoomInfo(int bookId)
        {
            return Ok(new
            {
                UserCount = Rooms.Count(kvp => kvp.Value == bookId)
            });
        }

        [HttpGet("/book/{bookId}/ws")]
        public async Task Connect(int bookId)
        {
            HttpContext context = ControllerContext.HttpContext;

            if (!context.WebSockets.IsWebSocketRequest)
            {
                context.Response.StatusCode = 400;
                return;
            }

            WebSocket websocket = await context.WebSockets.AcceptWebSocketAsync();
            logger.LogInformation("Added socket connection");
            Rooms.TryAdd(websocket, bookId);
            await MessageLoop(bookId, websocket);
            SendMessageToRoom(bookId, "User joined");
        }

        private async Task MessageLoop(int bookId, WebSocket websocket)
        {
            for(;;)
            {
                if (websocket.State == WebSocketState.CloseReceived)
                {
                    await CloseSocket(websocket);
                    await Task.WhenAll(SendMessageToRoom(bookId, "User left"));
                    return;
                }
                
                ArraySegment<byte> data = new ArraySegment<byte>(new byte[4096]);
               
                WebSocketReceiveResult result = await websocket.ReceiveAsync(data, CancellationToken.None);
                logger.LogInformation(Encoding.UTF8.GetString(data));
                await Task.WhenAll(SendMessageToRoom(bookId,  data[..result.Count]));
            }
        }
        
        private IEnumerable<Task> SendMessageToRoom(int bookId, object data) => SendMessageToRoom(bookId, JsonConvert.SerializeObject(data));
        private IEnumerable<Task> SendMessageToRoom(int bookId, string data) => SendMessageToRoom(bookId, Encoding.UTF8.GetBytes(data));
        private IEnumerable<Task> SendMessageToRoom(int bookId, ArraySegment<byte> data)
        {
            foreach ((WebSocket socket, int _) in Rooms.Where(kvp => kvp.Value == bookId))
            {
                yield return socket.SendAsync(data, WebSocketMessageType.Text, true,
                    CancellationToken.None);
            }
        }

        private Task CloseSocket(WebSocket socket)
        {
            logger.LogInformation("Closing socket");
            Rooms.Remove(socket, out int _);
            return socket.CloseAsync(WebSocketCloseStatus.Empty, null, CancellationToken.None);
        }
    }
}