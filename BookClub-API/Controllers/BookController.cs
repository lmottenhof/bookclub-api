using System;
using System.Linq;
using System.Threading.Tasks;
using BookClubLogic.Interfaces;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BookClub_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BookController : ControllerBase
    {
        private IBookLogic bookLogic;
        private ILogger<BookController> logger;
        
        public BookController(ILogger<BookController> logger, IBookLogic bookLogic)
        {
            this.bookLogic = bookLogic;
            this.logger = logger;
        }

        [HttpGet("page/{page:int}")]
        public async Task<IActionResult> GetBooks(uint page)
        {
            try
            {
                return Ok(new
                {
                    Page = page,
                    MaxPage = this.bookLogic.GetPageCount(),
                    Books = this.bookLogic.GetBooks((int)page),
                    
                });
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex, "Exception");
                return BadRequest();
            }
        }

        [HttpGet("{bookId}")]
        public async Task<IActionResult> GetBookInformation(int bookId)
        {
            try
            {
                return Ok(this.bookLogic.GetDetail(bookId));
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex, "Exception");
                return BadRequest();
            }
        }
    }
}