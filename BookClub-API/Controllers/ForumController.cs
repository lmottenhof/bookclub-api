using System;
using System.Linq;
using System.Threading.Tasks;
using BookClubLogic.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Models;

namespace BookClub_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ForumController : ControllerBase
    {
        private readonly ILogger<ForumController> logger;
        private readonly IForumLogic forumLogic;
        
        public ForumController(ILogger<ForumController> logger, IForumLogic forumLogic)
        {
            this.logger = logger;
            this.forumLogic = forumLogic;
        }

        [HttpPost("/book/{bookId}/forum")]
        public IActionResult CreatePost(int bookId, ForumPostRequest request)
        {
            try
            {
                this.forumLogic.CreatePost(bookId, "testUser", request.Title, request.Text);
                return Ok();
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Exception");
                return BadRequest();
            }
        }

        [HttpGet("/book/{bookId}/forum/{page}")]
        public IActionResult GetForumPosts(int bookId, int page)
        {
            try
            {
                return Ok(new
                {
                    Page = page,
                    MaxPage = this.forumLogic.GetPageCount(bookId),
                    posts = this.forumLogic.GetForumPosts(bookId, page).Select(fp => new
                    {
                        fp.Id,
                        fp.Title,
                        fp.Text,
                        PostTime = fp.PostTime.ToString("dddd dd MMMM HH:mm"),
                        fp.Author
                    })
                });
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Exception" );
                return BadRequest();
            }
        }
    }
}