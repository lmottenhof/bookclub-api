﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using BookClubData.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using Models;
using Models.Requests;
using ORM;
using ORM.Repositories;

namespace BookClubData.Implementations
{
    public class BookRepository :  IBookRepository
    {
        private readonly int pageSize;
        private readonly DatabaseContext context;
        public BookRepository(DatabaseContext context, int pageSize)
        {
            this.context = context;
            this.pageSize = pageSize;
        }
        public Book? GetById(int id)
        {
            return this.context.Query<Book>("SELECT * FROM books WHERE Id = @Id", ("Id", id)).SingleOrDefault();
        }

        public IEnumerable<Book> GetPage(int page)
        {
            return this.context.Query<Book>("SELECT * FROM books LIMIT @pageSIze OFFSET @offset",
                ("offset", page * pageSize),
                ("pageSize", pageSize));
        }

        public int GetPageCount()
        {
            return Convert.ToInt32(this.context.Scalar("SELECT floor((SELECT COUNT(*) FROM books) / @pageSize) - 1",
                ("pageSize", pageSize)));
        }
    }
}
