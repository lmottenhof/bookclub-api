﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Models;
using ORM;

namespace BookClubData.Implementations
{
   
    public class MockBookRepository : IRepository<Book>
    {
        public MockBookRepository()
        {
        }
        
        private readonly List<Book> mockBooks = new List<Book> {
            new Book
            {
                Id = 1,
                Title = "A Brief history of Time",
                Authors = "Stephen Hawking",
                //Thumbnail = "https://cdn.waterstones.com/bookjackets/large/9780/8575/9780857501004.jpg"
            }
        };

        public IEnumerable<Book> Read()
        {
            return mockBooks;
        }

        public int Update(Book obj)
        {
            this.mockBooks[this.mockBooks.IndexOf(this.mockBooks.Single(b => b.Id == obj.Id))] = obj;
            return 1;
        }

        public int Delete(Book obj)
        {
            this.mockBooks.Remove(this.mockBooks.Find(b => b.Id == obj.Id));
            return 1;
        }

        public int Insert(Book obj)
        {
            this.mockBooks.Add(obj);
            return 1;
        }
    }
}
