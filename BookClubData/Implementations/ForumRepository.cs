using System;
using System.Collections.Generic;
using BookClubData.Interfaces;
using Models;
using ORM;

namespace BookClubData.Implementations
{
    public class ForumRepository : IForumRepository
    {
        private readonly DatabaseContext context;
        private readonly int pageSize;
        
        public ForumRepository(DatabaseContext context, int pageSize)
        {
            this.context = context;
            this.pageSize = pageSize;
        }

        public void Insert(int bookId, string author, string title, string text, DateTime postTime)
        {
            this.context.Execute("INSERT INTO forumpost(title, text, posttime, author) VALUES(@title, @text, @posttime, @author)",
                ("title", title),
                ("text", text),
                ("posttime", postTime),
                ("author", author));
        }

        public void Delete(int bookId)
        {
            this.context.Execute("DELETE FROM forumpost WHERE bookid = @bookid", ("bookid", bookId));
        }

        public void Update(int bookId, string title, string text, DateTime postTime)
        {
            this.context.Execute("UPDATE forumpost SET title = @title, text = @text, posttime = @posttime WHERE bookid = @bookid",
                ("bookid", bookId),
                ("title", title),
                ("text", text),
                ("posttime", postTime));
        }

        public IEnumerable<ForumPost> GetPosts(int bookId, int page)
        {
            return this.context.Query<ForumPost>("SELECT * FROM forumpost WHERE bookid = @bookid ORDER BY posttime DESC LIMIT @pageSize OFFSET @offset",
                ("bookid", bookId),
                ("offset", pageSize * page),
                ("pageSize", pageSize));
        }

        public int GetPageCount(int bookId)
        {
            return Convert.ToInt32(this.context.Scalar("SELECT ceil((SELECT COUNT(*) FROM forumpost WHERE bookId = @bookId) / @pageSize)", 
                ("bookId", bookId),
                ("pageSize", pageSize)));
        }
    }
}