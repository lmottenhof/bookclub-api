using System;
using System.Collections.Generic;
using System.Linq;
using BookClubData.Interfaces;
using Models;
using Models.Requests;
using ORM;

namespace BookClubData.Implementations
{
    public class ReviewRepository : IReviewRepository
    {
        private readonly int pageSize;
        private readonly DatabaseContext context;
        public ReviewRepository(DatabaseContext context, int pageSize)
        {
            this.context = context;
            this.pageSize = pageSize;
        }

        public IEnumerable<BookReview> GetByBookId(int page, int bookId)
        {
            return this.context.Query<BookReview>("SELECT * FROM review WHERE BookId = @id  ORDER BY posttime DESC LIMIT @pageSize OFFSET @offset",
                ("id", bookId),
                ("offset", pageSize * page),
                ("pageSize", pageSize));
        }
        
        public void Insert(string userId, int bookId, ReviewRequest request, DateTime postTime)
        {
            this.context.Execute("INSERT INTO review(bookId, text, rating, author, posttime) VALUES (@bookId, @text, @rating, @author, @postTime)",
                ("bookId", bookId),
                ("text", request.ReviewText),
                ("rating", request.Rating),
                  ("author", userId),
              ("postTime", postTime));
        }

        public void Delete(int id)
        {
            this.context.Execute("DELETE review WHERE Id = @id", ("id", id));
        }

        public ReviewData? GetReviewData(int bookId)
        {
            return this.context.Query<ReviewData>("SELECT COUNT(rating) AS ReviewCount, round(AVG(rating), 2) AS AverageRating FROM review WHERE bookid = @bookId", ("bookId", bookId)).FirstOrDefault();
        }
        
        public int GetPageCount(int bookId)
        {
            return Convert.ToInt32(this.context.Scalar("SELECT ceil((SELECT COUNT(*) FROM review WHERE bookId = @bookId) / @pageSize)", 
                ("bookId", bookId),
                ("pageSize", pageSize)));
        }
    }
}