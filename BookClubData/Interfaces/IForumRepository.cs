using System;
using System.Collections.Generic;
using Models;

namespace BookClubData.Interfaces
{
    public interface IForumRepository
    {


        void Insert(int bookId, string author, string title, string text, DateTime postTime);
        void Delete(int bookId);
        void Update(int bookId, string title, string text, DateTime postTime);
        IEnumerable<ForumPost> GetPosts(int bookId, int page);
        int GetPageCount(int bookId);
    }
}