using System.Collections.Generic;
using Models;
using ORM;

namespace BookClubData.Interfaces
{
    public interface IBookRepository
    {
        IEnumerable<Book> GetPage(int page);
        Book? GetById(int id);
        int GetPageCount();
    }
}