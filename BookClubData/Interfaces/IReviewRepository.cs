using System;
using System.Collections.Generic;
using Models;
using Models.Requests;
using ORM;

namespace BookClubData.Interfaces
{
    public interface IReviewRepository 
    {
        IEnumerable<BookReview> GetByBookId(int page, int bookId);
        void Insert(string userId, int bookId, ReviewRequest request, DateTime postTime);
        void Delete(int id);
        ReviewData? GetReviewData(int bookId);
        int GetPageCount(int bookId);
    }
}