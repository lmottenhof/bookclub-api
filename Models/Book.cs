﻿using System;

namespace Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Authors { get; set; }
        public string Title { get; set; }
        public string Isbn { get; set; }
        public long Isbn13 { get; set; }
        public int PageCount { get; set; }
        public string Language { get; set; }

    }
}
