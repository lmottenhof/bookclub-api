using System;

namespace Models
{
    public class BookReview
    {
        public int Id { get; set; }
        public int BookId { get; set; }
        public int Rating { get; set; }
        public string Text { get; set; }
        public string Author { get; set; }
        public DateTime PostTime { get; set; }
    }
}