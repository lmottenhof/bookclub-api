namespace Models
{
    public class ReviewData
    {
        public decimal AverageRating { get; set; }
        public long ReviewCount { get; set; }
    }
}