namespace Models
{
    public class ForumPostRequest
    {
        public string Title { get; set; }
        public string Text { get; set; }
    }
}