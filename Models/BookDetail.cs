namespace Models
{
    public class BookDetail
    {
        public int Id { get; set; }
        public string Authors { get; set; }
        public string Title { get; set; }
        public string Isbn { get; set; }
        public long Isbn13 { get; set; }
        public int PageCount { get; set; }
        public string Language { get; set; }
        public decimal AverageRating { get; set; }
        public long ReviewCount { get; set; }
    }
}