namespace Models.Requests
{
    public class ReviewRequest
    {
        public string ReviewText { get; set; }
        public int Rating { get; set; }
    }
}